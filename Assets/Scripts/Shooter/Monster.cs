﻿using System.Collections;
using System.Collections.Generic;
using UniLua;
using UnityEngine;
using UnityEngine.AI;

namespace Shooter
{
    public class Monster : Enemy
    {
        const float AGGRO_COOLDOWN = 1f;

        bool _aggro = false;
        bool _attackRange = false;


        NavMeshAgent _agent;
        float _aggroTimer = 0;


        void Start()
        {
            Type = "shooter";
            _pl = FindObjectOfType<PlayerController>();
            _agent = GetComponent<NavMeshAgent>();
        }

        public override void Hit(float value)
        {
            _aggro = true;
            health -= value;
            if (health <= 0)
            {

            }
        }

        public void OnTriggerEnter()
        {
            _attackRange = true;
        }

        public void OnTriggerExit()
        {
            _attackRange = false;
        }

        protected override int Shooter_Attack(ILuaState s)
        {
            var attackDamage = (float)s.L_CheckNumber(1);
            Attack(attackDamage);
            return 1;
        }

        void Update()
        {
            Aggro();
        }

        void Attack(float attackDamage)
        {
            if (!_attackRange)
                return;

            if (_pl != null)
            {
                _pl.Hit(attackDamage);
            }
        }

        void Aggro()
        {
            _aggroTimer += Time.deltaTime;
            if (_aggro)
            {
                if (_aggroTimer > AGGRO_COOLDOWN)
                {
                    _agent.SetDestination(_pl.transform.position);
                }
            }
            else
            {
                if (_aggroTimer > AGGRO_COOLDOWN)
                {
                    _aggroTimer = 0;

                    RaycastHit hit;
                    Physics.Raycast(transform.position, _pl.transform.position - transform.position, out hit, 1000f, LayerMask.GetMask("Player") | LayerMask.GetMask("Default"));

                    if (hit.collider != null && hit.collider.GetComponent<PlayerController>())
                    {
                        Debug.DrawLine(transform.position, hit.point, Color.blue, 1f);
                        _aggro = true;

                        LoadScript("monster");
                    }
                }
            }
        }
    }
}