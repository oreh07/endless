﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Shooter
{
    public class PlayerController : MonoBehaviour
    {
        private Rigidbody rigidbody;

        [SerializeField] Camera camera;
        [SerializeField] Animator animator;
        [SerializeField] Animator moveAnimator;
        [SerializeField] Animator redScreenAnimator;
        [SerializeField] Animator cameraAnimator;
        [SerializeField] Transform hitEffect;
        [SerializeField] float forward;
        [SerializeField] float rotate;
        [SerializeField] float healthMax = 30f;
        [SerializeField] float _health = 30f;
        [SerializeField] float hitDamage = 0.5f;
        [SerializeField] Image redScreen;
        [SerializeField] Sprite[] hits;

        private const float REGENERATION = 0.3f;
        private const float EFFECT_DELAY = 0.01f;
        private const float SHOOT_DELAY = 0.1f;
        private float _shotTimer = 0;
        private float _redEffectTimer = 0;
        private bool _isShoot = false;

        private bool IsAlive {
            get {
                return _health > 0;
            }
        }

        void Awake()
        {
            rigidbody = GetComponentInChildren<Rigidbody>();

        }

        void Update()
        {
            if (!IsAlive) return;

            if (_shotTimer > EFFECT_DELAY)
            {
                hitEffect.gameObject.SetActive(false);
            }

            _health = Mathf.Min(_health + REGENERATION * Time.deltaTime, healthMax);
            redScreen.color = new Color(1f, 0f, 0f, 1f - _health / healthMax * 0.8f - 0.2f);

            _shotTimer += Time.deltaTime;
            _isShoot = Input.GetKey(KeyCode.Space);
            animator.SetBool("shoot", _isShoot);
            if (_isShoot)
            {
                Shoot();
            }
        }

        void Shoot()
        {
            if (_shotTimer >= SHOOT_DELAY)
            {
                _shotTimer = 0;

                RaycastHit hit;
                Physics.Raycast(camera.transform.position, transform.forward, out hit, 1000f, LayerMask.GetMask("Enemy") | LayerMask.GetMask("Default"));

                if (hit.collider != null && hit.collider.GetComponent<Billboard>())
                {
                    hitEffect.transform.position = hit.point + (camera.transform.position - hit.point).normalized * 0.1f;
                    hitEffect.gameObject.SetActive(true);
                    hitEffect.GetComponent<SpriteRenderer>().sprite = hits[Random.Range(0, hits.Length)];
                    Debug.DrawLine(camera.transform.position, hit.point, Color.red);

                    hit.collider.GetComponentInParent<Enemy>().Hit(hitDamage);
                }
            }
        }

        public void Hit(float value)
        {
            _health -= value;
            if (!IsAlive)
            {
                MenuController.Instance.OnDeath();
            }
            cameraAnimator.SetFloat("health", _health);
            redScreenAnimator.SetTrigger("0");
        }

        void LateUpdate()
        {
            if (!IsAlive) return;

            rigidbody.AddForce(transform.forward * forward * Input.GetAxis("Vertical"), ForceMode.VelocityChange);
            rigidbody.AddTorque(Vector3.up * rotate * Input.GetAxis("Horizontal"));

            Debug.DrawLine(transform.position, transform.position + transform.forward);

            moveAnimator.SetFloat("speed", rigidbody.velocity.magnitude);

        }
    }
}
