﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shooter
{
    public class MenuController : MonoBehaviour
    {
        public static MenuController _instance;
        public static MenuController Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<MenuController>();
                }
                return _instance;
            }
        }

        [SerializeField] GameObject _menuScreen;

        public void OnDeath()
        {
            //_menuScreen.SetActive(true);

        }


    }
}

