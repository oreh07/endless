﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shooter
{
    [RequireComponent(typeof(Animator))]
    public class Door : MonoBehaviour
    {
        Animator doorAnimation;

        void Awake()
        {
            doorAnimation = GetComponent<Animator>();
        }


        public void OnPlayerEnter()
        {
            doorAnimation.SetBool("open", true);

        }
        public void OnPlayerExit()
        {
            doorAnimation.SetBool("open", false);
        }
    }
}
