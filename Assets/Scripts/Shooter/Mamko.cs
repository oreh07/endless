﻿using System.Collections;
using System.Collections.Generic;
using UniLua;
using UnityEngine;

namespace Shooter
{
    public class Mamko : Enemy
    {
        [SerializeField] Transform[] enemySpawnPositions;
        [SerializeField] GameObject monsterPrefab;
        [SerializeField] GameObject eggPrefab;

        void Start()
        {
            Type = "shooter";
            _pl = FindObjectOfType<PlayerController>();
        }
        
        public void TRIGGERED()
        {
            LoadScript("mamko");
        }

        protected override int Shooter_GetPlayerPositionX(ILuaState s)
        {
            s.PushNumber(_pl.transform.position.x);
            return 1;
        }

        protected override int Shooter_GetPlayerPositionY(ILuaState s)
        {
            s.PushNumber(_pl.transform.position.y);
            return 1;
        }

        protected override int shooter_SpawnEgg(ILuaState s)
        {
            var egg = Instantiate<GameObject>(eggPrefab, transform.position, Quaternion.identity);

            Debug.Log("spawn egg");

            return 1;
        }

        protected override int shooter_SpawnMonster(ILuaState s)
        {
            var pos = enemySpawnPositions[Random.Range(0, enemySpawnPositions.Length - 1)];

            var monster = Instantiate<GameObject>(monsterPrefab, pos.position, Quaternion.identity);


            Debug.Log("spawn monster");

            return 1;
        }

        protected override int shooter_GetHealth(ILuaState s)
        {
            s.PushNumber(health / healthMax);
            return 1;
        }

    }
}
