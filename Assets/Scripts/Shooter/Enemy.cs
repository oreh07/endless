﻿using UnityEngine;

namespace Shooter
{
    public class Enemy : LuaResponder
    {
        protected PlayerController _pl;

        [SerializeField] protected float healthMax = 100f;
        [SerializeField] protected float health = 15f;
        [SerializeField] protected SpriteRenderer image;
        [SerializeField] protected Sprite[] sprites;


        public virtual void Hit(float value) {}
    }
}