﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Puzzle
{
    public class PuzzleItem : MonoBehaviour
    {
        private int _value;
        public int Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
                text.text = _value.ToString();
            }
        }
        public Vector2 Position
        {
            get
            {
                int x = Mathf.FloorToInt(transform.localPosition.x / 256f);
                int y = Mathf.FloorToInt(transform.localPosition.y / 256f);
                return new Vector2(x, -y);
            }
        }

        public Text text;

        public Action<int> OnButtonPress;

        public void ButtonPress()
        {
            OnButtonPress.Invoke(Value);
        }

    }
}
