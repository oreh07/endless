﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Puzzle
{
    public class PuzzleController : MonoBehaviour
    {
        public GameObject puzzleItem;
        public Transform puzzleContainer;
        public List<PuzzleItem> items = new List<PuzzleItem>();

        public Vector2 empty;

        private List<int> queue = new List<int>();
        private Tween move;

        void Start()
        {
            List<int> list = Generate();

            for (int i = 0; i < 15; i++)
            {
                PuzzleItem item = Instantiate(puzzleItem, puzzleContainer).GetComponent<PuzzleItem>();
                item.Value = list[i];
                item.OnButtonPress += OnPress;
                item.transform.localPosition = GetPos(i);
                items.Add(item);
                Debug.LogFormat("{0} {1}", list[i], GetPos(list[i]));
            }

            empty = new Vector2(3f, 3f);
            move = transform.DOMove(transform.position, 0.1f);
        }

        private List<int> Generate()
        {
            List<int> list = new List<int>(15);
            int check;

            do
            {
                check = 4;
                list.Clear();
                List<int> nums = new List<int>(15) { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
                for (int i = 0; i < 15; i++)
                {
                    int n = Random.Range(0, nums.Count);
                    list.Add(nums[n]);
                    nums.RemoveAt(n);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    for (int j = 0; j < i; j++)
                    {
                        if (list[j] > list[i])
                        {
                            check++;
                        }
                    }
                }
            } while (check % 2 != 0);
            Debug.Log("Check " + check);

            return list;
        }

        public void OnPress(int n)
        {
            if (IsNear(n))
            {
                if (move.IsPlaying())
                {
                    queue.Add(n);
                    return;
                }
                //Debug.LogFormat("can move from {0} to {1}", GetItem(n).Position, empty);
                Vector3 res = ((GetItem(n).Position - empty) * 256f);
                res.x *= -1;
                move = GetItem(n).transform.DOMove(GetItem(n).transform.position + res, 0.3f).OnComplete(OnMoveEnd);  //
                empty = GetItem(n).Position;
            }
            //Debug.Log("press " + n + " v " + GetItem(n).Position);
        }

        private void OnMoveEnd()
        {
            //move.Kill();
            //if (queue.Count > 0)
            //{
            //    OnPress(queue[0]);
            //    queue.RemoveAt(0);
            //}
            CheckWin();
        }

        private void CheckWin()
        {
            bool c = true;
            for (int i = 0; i < items.Count && c; i++)
            {
                var p = items[i].Position;
                p.y *= 1f;
                Vector2 pos = new Vector2((items[i].Value - 1) % 4, Mathf.FloorToInt((items[i].Value - 1) / 4));
                c = pos == p;
            }
            Debug.Log(c);
        }

        public bool IsNear(int a)
        {
            Vector2 dv = GetItem(a).Position - empty;
            if (Mathf.Abs(dv.x) > 1 || Mathf.Abs(dv.y) > 1)
                return false;

            bool nearx = dv.x == 1f || dv.x == -1f;
            bool neary = dv.y == 1f || dv.y == -1f;
            return nearx && !neary || !nearx && neary;
        }

        PuzzleItem GetItem(int n)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].Value == n)
                {
                    return items[i];
                }
            }
            return items[0];
        }

        Vector2 GetPos(int i)
        {
            int y = i % 4;
            int x = Mathf.FloorToInt(i / 4);
            return new Vector2(x * 256, -y * 256);
        }
    }
}