﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnTrigger : MonoBehaviour
{
    [SerializeField] LayerMask _mask;
    [SerializeField] UnityEvent _onTriggerEnter;
    [SerializeField] UnityEvent _onTriggerExit;

    private void OnTriggerEnter(Collider other)
    {
        if ((_mask & 1 << other.gameObject.layer) > 0)
        {
            _onTriggerEnter.Invoke();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if ((_mask & 1 << other.gameObject.layer) > 0)
        {
            _onTriggerExit.Invoke();
        }
    }

}
