﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    Camera _cam;
    private void Start()
    {
        _cam = Camera.main;
    }

    void Update()
    {
        transform.rotation = _cam.transform.rotation;

    }
}
