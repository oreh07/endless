﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UniLua;

public class LuaResponder : MonoBehaviour
{
    protected string luaScript;
    protected ILuaState luaState;
    protected ThreadStatus luaStatus;
    protected ILuaState threadLuaState;

    public static Dictionary<string, string> responderStringDict = new Dictionary<string, string>();
    public static Dictionary<string, double> responderNumberDict = new Dictionary<string, double>();

    public static string Type;

    protected void LoadScript(string name)
    {
        string url = string.Concat("lua/", Type, "/", name); //, ".lua"
        //print("Load lua script: " + url);

        TextAsset targetFile = Resources.Load<TextAsset>(url);
        string luaText = "";
        try
        {
            luaText = targetFile.text;
        }
        catch (System.Exception ex)
        {
            Debug.LogError("loading file exception: " + ex.Message);
            Debug.LogError("url: " + url);
            return;
        }

        if (luaText.Length > 0)
        {
            if (luaState != null)
            {
                //print(threadLuaState.GetTop());
                threadLuaState.Pop(threadLuaState.GetTop());
            }

            luaState = LuaAPI.NewState();
            luaState.L_OpenLibs();
            luaState.L_RequireF("libs", Libs, true);

            luaScript = luaText;

            threadLuaState = luaState.NewThread();
            luaStatus = threadLuaState.L_LoadString(luaScript);

            if (luaStatus != ThreadStatus.LUA_OK)
            {
                Debug.LogError(url + " not loaded. Status:" + luaStatus.ToString());
            }

            threadLuaState.Resume(null, 0);
        }
        else
        {
            Debug.LogWarning("file not found " + url);
            luaState = null;
            threadLuaState = null;
        }
    }

    IEnumerator StartWaiting(double time)
    {
        yield return new WaitForSeconds((float)time);
        threadLuaState.Resume(null, 0);
    }

    #region lib functions
    #region other
    protected virtual int LuaTrace(ILuaState s)
    {
        Debug.Log("ltrace: " + s.L_CheckString(1));
        return 1;
    }
    protected virtual int SetVariable(ILuaState s)
    {
        string key = s.L_CheckString(1);

        if (s.Type(2) == UniLua.LuaType.LUA_TSTRING)
        {
            if (responderStringDict.ContainsKey(key))
            {
                responderStringDict.Remove(key);
            }
            responderStringDict.Add(key, s.L_CheckString(2));
        }
        else
        {
            if (responderNumberDict.ContainsKey(key))
            {
                responderNumberDict.Remove(key);
            }
            responderNumberDict.Add(key, s.L_CheckNumber(2));
        }

        return 1;
    }
    protected virtual int GetVariable(ILuaState s)
    {
        //Debug.Log("  getVariable "+s.L_CheckString(1));
        string key = s.L_CheckString(1);

        if (responderStringDict.ContainsKey(key))
        {
            s.PushString(responderStringDict[key]);
        }
        else if (responderNumberDict.ContainsKey(key))
        {
            s.PushNumber(responderNumberDict[key]);
        }
        else
        {
            s.PushInteger(0);
        }
        return 1;
    }
    protected virtual int GetRangom(ILuaState s)
    {
        s.PushNumber(Random.Range(0f, 1f));
        return 1;
    }
    protected virtual int LoadScriptLua(ILuaState s)
    {
        LoadScript(s.L_CheckString(1));
        return 1;
    }
    protected virtual int Waiting(ILuaState s)
    {
        StartCoroutine(StartWaiting(s.L_CheckNumber(1)));
        return s.YieldK(s.GetTop(), 0, null);
    }
    #endregion
    #region shooter
    protected virtual int Shooter_Attack(ILuaState s) { return 1; }
    protected virtual int Shooter_GetPlayerPositionX(ILuaState s) { return 1; }
    protected virtual int Shooter_GetPlayerPositionY(ILuaState s) { return 1; }
    protected virtual int shooter_SpawnEgg(ILuaState s) { return 1; }
    protected virtual int shooter_SpawnMonster(ILuaState s) { return 1; }
    protected virtual int shooter_GetHealth(ILuaState s) { return 1; }
    #endregion
    #endregion

    protected int Libs(ILuaState lua)
    {
        var define = new NameFuncPair[]{

            //dialogue
            new NameFuncPair("loadScript", LoadScriptLua),
            
            //actions
            new NameFuncPair("getRandom", GetRangom),
            new NameFuncPair("waiting", Waiting),
            
            //shooter
            new NameFuncPair("shooter_attack", Shooter_Attack),
            new NameFuncPair("shooter_getPlayerPositionX", Shooter_GetPlayerPositionX),
            new NameFuncPair("shooter_getPlayerPositionY", Shooter_GetPlayerPositionY),
            new NameFuncPair("shooter_spawnEgg", shooter_SpawnEgg),
            new NameFuncPair("shooter_spawnMonster", shooter_SpawnMonster),
            new NameFuncPair("shooter_getHealth", shooter_GetHealth),




            //other
            new NameFuncPair("trace", LuaTrace),
            new NameFuncPair("setVariable", SetVariable),
            new NameFuncPair("getVariable", GetVariable)
        };

        lua.L_NewLib(define);
        return 1;
    }
}